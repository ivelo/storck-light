from django.apps import AppConfig


class AlignmentConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'alignment'
