from django.db import models
from equalisation.models import Equalisation

class TimeAlignment(models.Model):
    date = models.CharField(max_length = 30, default = '-')
    comment = models.CharField(max_length = 150, default = '-')
    run = models.PositiveIntegerField()
    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

    bxidref = models.PositiveIntegerField()
    path = models.CharField(max_length = 300, default = '')
    equalisation = models.ForeignKey(Equalisation, on_delete = models.SET_NULL, null = True)
    
class AsicTimeAlignment(models.Model):
    takey = models.ForeignKey(TimeAlignment, on_delete = models.SET_NULL, null = True)
    asic = models.PositiveIntegerField()
    bxid_mean = models.FloatField(null = True)
    bxid_std = models.FloatField(null = True)
    bxid_min = models.FloatField(null = True)
    bxid_max = models.FloatField(null = True)
    csv_path = models.CharField(max_length = 300, null = True)
    bxid_hist_path = models.CharField(max_length = 300, null = True)
