from rest_framework import serializers 
from alignment.models import *
 
class TimeAlignmentSerializer(serializers.ModelSerializer):
    equalisation = serializers.CharField(read_only = True)
    class Meta:
        model = TimeAlignment
        fields = ('id',
                  'run',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment',
                  'bxidref',
                  'path',
                  'equalisation')

class AsicTimeAlignmentSerializer(serializers.ModelSerializer):
    takey = serializers.CharField(read_only = True)
    class Meta:
        model = AsicTimeAlignment
        fields = ('takey',
                  'asic',
                  'bxid_mean',
                  'bxid_std',
                  'bxid_max',
                  'bxid_min',
                  'csv_path',
                  'bxid_hist_path')