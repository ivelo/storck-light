from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from alignment.models import TimeAlignment, AsicTimeAlignment
from equalisation.models import Equalisation
from alignment.serializers import *
from rest_framework.decorators import api_view
from datetime import datetime

@api_view(['GET', 'POST'])
def api_ta(request):
    if request.method == 'GET':
        print(request)
        equals = TimeAlignment.objects.all().order_by('-id')
        #used mostly for Dashboard filling:
        type = request.GET.get('type')
        id = request.GET.get('id')
        if type and id:
            ata = AsicTimeAlignment.objects.filter(takey__id = id)
            atas = AsicTimeAlignmentSerializer(ata, many=True)
            response =  JsonResponse(atas.data, safe=False)
            return response

        ta = TimeAlignmentSerializer(equals, many=True)
        response = JsonResponse(ta.data, safe=False)
        return response
    elif request.method == 'POST':
        print(request)
        ta_data = JSONParser().parse(request)
        ta = TimeAlignmentSerializer(data=ta_data)
        if ta.is_valid():
            ta_saved = ta.save()
            ta_saved.equalisation = Equalisation.objects.filter(name = ta_data['equalisation'])[0]
            ta_saved.save()
            return JsonResponse(ta.data, status=status.HTTP_201_CREATED)
        return JsonResponse(ta.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def api_asicta(request):
    if request.method == 'POST':
        print(request)
        data = JSONParser().parse(request)
        print(data)
        ta = AsicTimeAlignmentSerializer(data = data)
        
        print(ta)
        if ta.is_valid():
            ta = ta.save()
            ta.takey = TimeAlignment.objects.all().order_by('-id')[0]
            ta.save()
            print('saving')
        else:
            return JsonResponse(ta.errors, status=status.HTTP_400_BAD_REQUEST)
        return JsonResponse({}, safe = False, status=status.HTTP_201_CREATED)

@api_view(['GET'])
def get_custom(request):
    db_id = request.GET.get('dbid')
    vp_id = request.GET.get('vpid')
    param = request.GET.get('param')
    type = request.GET.get('type')

    if type == None or type == '':
        ta = TimeAlignment.objects.filter(id = db_id)[0]
        ta_param_json = {param: getattr(ta, param)}
        if param == 'equalisation':
            ta_param_json = {param: getattr(ta, param).id}
    elif type == 'asic':
        ta = AsicTimeAlignment.objects.filter(takey__id = db_id).filter(asic = vp_id)[0]
        ta_param_json = {param: getattr(ta, param)}
    response =  JsonResponse(ta_param_json, safe=False)
    return response