from django.db import models
from run.models import Run

# Create your models here.
class Clusters(models.Model):
    #type of recipe
    #partitionname = models.CharField(max_length = 30, default = 'unspecified')
    #date of creation
    runkey = models.ForeignKey(Run, on_delete = models.SET_NULL, null = True)
    #runtype = models.CharField(max_length = 30, default = 'unspecified')
    #destination = models.CharField(max_length = 30, default = 'unspecified')
    #activity = models.CharField(max_length = 50, default = 'unspecified')
    #LHCState = models.CharField(max_length = 30, default = 'unspecified')
    #starttime = models.CharField(max_length = 30, default = '', null = True)
    #endtime = models.CharField(max_length = 30, default = '', null = True)
    #analysed = models.BooleanField(default = False)
    #comment displayed in the recipe viewer
    #comment = models.CharField(max_length = 200, default = '')

    #path_perasic_nobeam = models.CharField(max_length = 300)

    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

    class Meta: 
        app_label = 'clusters'

class ClustersAsic(models.Model):
    clusterskey = models.ForeignKey(Clusters, on_delete = models.SET_NULL, null = True)

    asic = models.IntegerField()

    nobeam_average = models.IntegerField(default = None, null = True)
    

    class Meta:
        app_label = 'clusters'