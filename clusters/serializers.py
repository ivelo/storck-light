from rest_framework import serializers 
from clusters.models import *

class ClustersSerializer(serializers.ModelSerializer):
    class Meta:
        model = Clusters
        fields = ('runkey',
                  #'path_perasic_nobeam',
                  'uploaded_by',
                  'uploaded_from')
        
class ClustersAsicSerializer(serializers.ModelSerializer):
    class Meta:
        model = ClustersAsic
        fields = ('clusterskey',
                  'asic',
                  'nobeam_average',)