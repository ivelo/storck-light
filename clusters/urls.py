from django.urls import path, re_path, include
from clusters import views 
 
urlpatterns = [ 
    re_path(r'^api/clusters$', views.api_clusters),
    re_path(r'^api/clustersasic$', views.api_clustersasic),
]
