from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
#from recipe.models import HwRecipe
from run.models import Run
from clusters.serializers import *
from rest_framework.decorators import api_view

@api_view(['GET', 'POST'])
def api_clusters(request):
    print('Entering clusters')
    if request.method == 'GET':
        print(request)
        data = Clusters.objects.all().order_by('-id')
        
        run = request.GET.get('run', None)
        if run is not None:
            data = data.filter(runkey__runid = run)
        
        serializer = ClustersSerializer(data, many=True)
        response =  JsonResponse(serializer.data, safe=False)
        return response

    elif request.method == 'POST':
        print(request)
        print('Addding a cluster abstract object')
        data = JSONParser().parse(request)

        runid = data['runid']
        del data['runid']

        serializer = ClustersSerializer(data=data)
        print(data, serializer)
        
        if_exists = Clusters.objects.filter(runkey__runid = runid)
        if if_exists:
            return JsonResponse({}, safe = False, status = status.HTTP_201_CREATED)
        
        if serializer.is_valid():
            query_object = serializer.save()
            query_object.runkey = Run.objects.filter(runid=runid)[0]
            query_object.save()
            for i in range(624):
                asic_object = ClustersAsic()
                asic_object.clusterskey = Clusters.objects.all().order_by('-id')[0]
                asic_object.asic = i
                asic_object.save()
            return JsonResponse(serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def api_clustersasic(request):
    print('Entering clustersasic')
    if request.method == 'GET':
        print(request)
        #data = ClustersAsic.objects.all().order_by('-id')
        
        id = request.GET.get('id', None)
        #if id is not None:
        #    data = data.filter(id = id)
        data = ClustersAsic.objects.filter(clusterskey__runkey = id).order_by('-id')

        serializer = ClustersAsicSerializer(data, many=True)
        response =  JsonResponse(serializer.data, safe=False)
        return response

    elif request.method == 'POST':
        print(request)
        print('Addding clustersasic abstract objects')
        data = JSONParser().parse(request)

        runid = data['runid']
        del data['runid']

        cluster = Clusters.objects.filter(runkey__runid = runid)[0]
        print(cluster.id)
        #asics = ClustersAsic.objects.filter(clusterskey__id = cluster.id)
        #serializer = ClustersSerializer(data=data)
        #print(data, serializer)
        #print(data['set'])
        #print(asics)
        #print(asics[0])
        print(data['set']['0'])
        for i in data['set']:
            #i = #int(i)
            #print(int(i))
            asic = ClustersAsic.objects.filter(clusterskey__id = cluster.id).filter(asic = int(i))[0]
            #test = ClustersAsic.objects.filter(asic = int(i))
            #test2 = ClustersAsic.objects.filter(asic = 0)
            #print(asic)
            #print(test)
            #print(test2)
            #print(data['set'][i])
            asic.nobeam_average = data['set'][i]
            asic.save()
            #asics[int(i)].nobeam_average = data['set'][i]
            #asics[int(i)].save()
        #if serializer.is_valid():
        #    query_object = serializer.save()
        #    query_object.runkey = Run.objects.filter(runid=runid)[0]
        #    query_object.save()
        #    for i in range(624):
        #        asic_object = ClustersAsic()
        #        asic_object.clusterskey = Clusters.objects.all().order_by('-id')[0]
        #        asic_object.asic = i
        #        asic_object.save()
        return JsonResponse({}, safe = False, status=status.HTTP_201_CREATED) 
        #return JsonResponse(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    