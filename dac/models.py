from django.db import models

class DacScan(models.Model):
    date = models.CharField(max_length = 30, default = '')
    comment = models.CharField(max_length = 150)
    name = models.CharField(max_length = 100)
    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

class Vdac(models.Model):
    type = models.CharField(max_length = 20)
    date = models.CharField(max_length = 30, default = '')
    VDa = models.FloatField()
    VDb = models.FloatField()
    rec = models.PositiveIntegerField()

class Idac(models.Model):
    type = models.CharField(max_length = 20)
    date = models.CharField(max_length = 30, default = '')
    VDa = models.FloatField()
    VDb = models.FloatField()
    VDc = models.FloatField()
    IDa = models.FloatField()
    IDb = models.FloatField()
    rec = models.PositiveIntegerField()

class AsicDacScan(models.Model):
    dacscankey = models.ForeignKey(DacScan, on_delete = models.SET_NULL, null = True)
    asic = models.PositiveIntegerField()

    vfbk = models.OneToOneField(Vdac, on_delete = models.SET_NULL, related_name = 'vfbk', null = True) 
    vincas = models.OneToOneField(Vdac, on_delete = models.SET_NULL, related_name = 'vincas', null = True) 
    vpreampcas = models.OneToOneField(Vdac, on_delete = models.SET_NULL, related_name = 'vpreampcas', null = True) 
    vcasdisc = models.OneToOneField(Vdac, on_delete = models.SET_NULL, related_name = 'vcasdisc', null = True) 

    ipreamp = models.OneToOneField(Idac, on_delete = models.SET_NULL, related_name = 'ipreamp', null = True) 
    idisc = models.OneToOneField(Idac, on_delete = models.SET_NULL, related_name = 'idisc', null = True) 
    ikrum = models.OneToOneField(Idac, on_delete = models.SET_NULL, related_name = 'ikrum', null = True) 
    ipixeldac = models.OneToOneField(Idac, on_delete = models.SET_NULL, related_name = 'ipixeldac', null = True) 

