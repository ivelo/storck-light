from rest_framework import serializers 
from dac.models import *
 
class DacScanSerializer(serializers.ModelSerializer):
    class Meta:
        model = DacScan
        fields = ('id',
                  'name',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment',)