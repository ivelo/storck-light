from django.urls import path, re_path, include
from dac import views 
 
urlpatterns = [ 
    re_path(r'^api/dac$', views.api_dacscan),
]
