from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from dac.models import DacScan, AsicDacScan, Vdac, Idac
from dac.serializers import *
from rest_framework.decorators import api_view

@api_view(['GET', 'POST'])
def api_dacscan(request):
    if request.method == 'GET':
        print(request)

        dacscans = DacScan.objects.all().order_by('-id')
        ds = DacScanSerializer(dacscans, many = True)
        response = JsonResponse(ds.data, safe = False)
        return response

    elif request.method == 'POST':
        dacscan = JSONParser().parse(request)
        ds = DacScanSerializer(data = dacscan)
        if ds.is_valid():
            ds.save()
            return JsonResponse(ds.data, status=status.HTTP_201_CREATED)
        return JsonResponse(ds.errors, status=status.HTTP_400_BAD_REQUEST)
        """
        #pass
        #equals = Equalisation.objects.all().order_by('-id')
        dacscans = DacScan.objects.all().order_by('-id')

        #used mostly for Dashboard filling:
        #stype = request.GET.get('type')
        #id = request.GET.get('id')
        #if stype and id:
        #    equals = ControlEqualisation.objects.filter(asickey__equalisationkey = id)
        #    es = QuickEqualisationSerializer(equals, many=True)
        #    response =  JsonResponse(es.data, safe=False)
        #    return response

        ds = (equals, many=True)
        #response =  JsonResponse(es.data, safe=False)
        #return response

    elif request.method == 'POST':
        equals_data = JSONParser().parse(request)
        es = EqualisationSerializer(data=equals_data)
        if es.is_valid():
            es.save()
            #fills the Equalisation model with 624 asic objects:
            for i in range(624):
                asic_object = AsicEqualisation()
                asic_object.equalisationkey = Equalisation.objects.all().order_by('-id')[0]
                asic_object.asic = i
                asic_object.save()
            return JsonResponse(es.data, status=status.HTTP_201_CREATED)
        return JsonResponse(es.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def api_quickequalisation(request):
    #GET request will be used to for history (perhaps)
    if request.method == 'GET':
        pass
        #quick = QuickEqualisation.objects.all().order_by('-id')
        #quick_serializer = QuickEqualisationSerializer(quick, many=True)
        #response =  JsonResponse(quick_serializer.data, safe=False)
        #return response

    elif request.method == 'POST':
        ls, hs = None, None
        quick_data = JSONParser().parse(request)
        
        name = quick_data['name']
        del quick_data['name']

        
        scanlowserializer = ScanSerializer(data = quick_data['scanlow'])
        if scanlowserializer.is_valid():
            ls = scanlowserializer.save()
        del quick_data['scanlow']

        scanhighserializer = ScanSerializer(data = quick_data['scanhigh'])
        if scanhighserializer.is_valid():
            hs = scanhighserializer.save()
        del quick_data['scanhigh']

        asic = quick_data['asic']
        del quick_data['asic']
        quick_serializer = QuickEqualisationSerializer(data=quick_data)
        if quick_serializer.is_valid():
            query_object = quick_serializer.save()
            query_object.scanlow = ls
            query_object.scanhigh = hs
            query_object.asickey = AsicEqualisation.objects.filter(asic=asic).filter(equalisationkey__name = name)[0]
            print(ls, hs)
            query_object.save()
            return JsonResponse(quick_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(quick_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
"""