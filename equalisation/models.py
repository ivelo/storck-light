from django.db import models
from recipe.models import HwSubRecipe, HwRecipe

##### Equalisation Models #####
class Scan(models.Model):
    asic = models.PositiveIntegerField()
    trim = models.PositiveIntegerField()
    bottom = models.PositiveIntegerField()
    top = models.PositiveIntegerField()
    step = models.PositiveIntegerField()
    date = models.CharField(max_length = 30, default = 'no date specified')
    HwSubRecipe = models.ForeignKey(HwSubRecipe, on_delete = models.SET_NULL, null = True)
    #noise properties that will come after analysis
    nmean = models.FloatField()
    nstd = models.FloatField()
    nstdodd = models.FloatField()
    nstdeven = models.FloatField()
    nsplit = models.FloatField()
    nrate = models.FloatField()

class Equalisation(models.Model):
    date = models.CharField(max_length = 30, default = '')
    comment = models.CharField(max_length = 150)
    name = models.CharField(max_length = 100)
    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')
    path = models.CharField(max_length = 300, default = '')

class AsicEqualisation(models.Model):
    equalisationkey = models.ForeignKey(Equalisation, on_delete = models.SET_NULL, null = True)
    asic = models.PositiveIntegerField()

class QuickEqualisation(models.Model):
    asickey = models.ForeignKey(AsicEqualisation, on_delete = models.SET_NULL, null = True)
    #asic repeated for ivelo dashboard time performance
    asic = models.PositiveIntegerField()

    scanlow = models.OneToOneField(Scan, on_delete = models.SET_NULL, related_name = 'scanqlow', null = True) 
    scanhigh = models.OneToOneField(Scan, on_delete = models.SET_NULL, related_name = 'scanqhigh', null = True) 
    path = models.CharField(max_length = 200)

    # the quick scan results:
    ipixeldac = models.PositiveIntegerField()
    rbottom0 = models.PositiveIntegerField()
    rtop0 = models.PositiveIntegerField()
    rbottomF = models.PositiveIntegerField()
    rtopF = models.PositiveIntegerField()
    dist = models.IntegerField()

class NormalEqualisation(models.Model):
    asickey = models.ForeignKey(AsicEqualisation, on_delete = models.SET_NULL, null = True)
    asic = models.PositiveIntegerField()

    scanlow = models.OneToOneField(Scan, on_delete = models.SET_NULL, related_name = 'scannlow', null = True) 
    scanhigh = models.OneToOneField(Scan, on_delete = models.SET_NULL, related_name = 'scannhigh', null = True) 
    path = models.CharField(max_length = 200)

    trim = models.FloatField()
    rbottom = models.PositiveIntegerField()
    rtop = models.PositiveIntegerField()
    dist = models.IntegerField()

class ControlEqualisation(models.Model):
    asickey = models.ForeignKey(AsicEqualisation, on_delete = models.SET_NULL, null = True)
    asic = models.PositiveIntegerField()

    scan = models.OneToOneField(Scan, on_delete = models.SET_NULL, related_name = 'scanhigh', null = True) 
    path = models.CharField(max_length = 200)

    masks = models.PositiveIntegerField()
    globalthreshold = models.PositiveIntegerField()