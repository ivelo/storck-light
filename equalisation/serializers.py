from rest_framework import serializers 
from equalisation.models import *
 
class EqualisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Equalisation
        fields = ('id',
                  'name',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment',
                  'path')

class ScanSerializer(serializers.ModelSerializer):
    class Meta:
        model = Scan
        fields = ('asic',
                  'trim',
                  'bottom',
                  'top',
                  'step',
                  'date',
                  'nmean',
                  'nstd',
                  'nstdodd',
                  'nstdeven',
                  'nsplit',
                  'nrate')

class AsicEqualisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = AsicEqualisation
        fields = ('equalisationkey',
                  'asic')

class QuickEqualisationSerializer(serializers.ModelSerializer):
    asickey = serializers.CharField(read_only = True)
    scanlow = ScanSerializer(read_only = True)
    scanhigh = ScanSerializer(read_only = True)
    class Meta:
        model = QuickEqualisation
        fields = ('id',
                  'asic',
                  'asickey',
                  'scanlow',
                  'scanhigh',
                  'path',
                  'ipixeldac',
                  'rbottom0',
                  'rtop0',
                  'rbottomF',
                  'rtopF',
                  'dist',)

class DashboardQuickEqualisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = QuickEqualisation
        fields = ('asic',
                  'ipixeldac',
                  'rbottom0',
                  'rtop0',
                  'rbottomF',
                  'rtopF',
                  'dist',)

class NormalEqualisationSerializer(serializers.ModelSerializer):
    asickey = serializers.CharField(read_only = True)
    scanlow = ScanSerializer(read_only = True)
    scanhigh = ScanSerializer(read_only = True)
    class Meta:
        model = NormalEqualisation
        fields = ('id',
                  'asic',
                  'asickey',
                  'scanlow',
                  'scanhigh',
                  'path',
                  'trim',
                  'rbottom',
                  'rtop',
                  'dist',)
        
class DashboardNormalEqualisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = NormalEqualisation
        fields = ('asic',
                  'trim',
                  'rbottom',
                  'rtop',
                  'dist',)

class ControlEqualisationSerializer(serializers.ModelSerializer):
    asickey = serializers.CharField(read_only = True)
    scan = ScanSerializer(read_only = True)
    class Meta:
        model = ControlEqualisation
        fields = ('id',
                  'asic',
                  'asickey',
                  'scan',
                  'path',
                  'masks',
                  'globalthreshold',)
        
class DashboardControlEqualisationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ControlEqualisation
        fields = ('asic',
                  'masks',
                  'globalthreshold',)
        
#class QuickPathSerializer(serializers.ModelSerializer):
#    path = serializers.CharField(read_only = True)
#    class Meta:
#        model = QuickEqualisation
#        fields = ('path',)