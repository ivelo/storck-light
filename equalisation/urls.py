from django.urls import path, re_path, include
from equalisation import views 
 
urlpatterns = [ 
    re_path(r'^api/equalisation$', views.api_equalisation),
    re_path(r'^api/equalisation/asic/quick$', views.api_quickequalisation),
    re_path(r'^api/equalisation/asic/normal$', views.api_normalequalisation),
    re_path(r'^api/equalisation/asic/control$', views.api_controlequalisation),
    re_path(r'^api/equalisation/scan$', views.api_scan),
    re_path(r'^api/equalisation/custom$', views.get_custom),
    #do we need the following apis?
    #re_path(r'^api/equalisation/asic$', views.api_asicequalisation),
    #re_path(r'^api/equalisation/asic/quick$', views.api_quickequalisation),


    #re_path(r'^api/recipe/subrecipe/hw/link$', views.api_hwrecipelink),
    #re_path(r'^api/recipe/pixel$', views.api_pixelrecipe),
    #re_path(r'^api/recipe/subrecipe/hw$', views.api_hwsubrecipe),
    #re_path(r'^api/recipe/subrecipe/pixel$', views.api_pixelsubrecipe),
]
