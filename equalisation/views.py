from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from equalisation.models import Equalisation, AsicEqualisation
from equalisation.serializers import *
from rest_framework.decorators import api_view
import time
#import asyncio

@api_view(['GET', 'POST'])
def api_equalisation(request):
    if request.method == 'GET':
        print(request)
        equals = Equalisation.objects.all().order_by('-id')
        es = None

        #used mostly for Dashboard filling:
        stype = request.GET.get('type')
        id = request.GET.get('id')
        scan = request.GET.get('scan')
        if stype and id:
            if stype == 'quick':
                if scan:
                    if scan == 'scanlow':
                        scan_objects = Scan.objects.filter(scanqlow__asickey__equalisationkey = id)
                    if scan == 'scanhigh':
                        scan_objects = Scan.objects.filter(scanqhigh__asickey__equalisationkey = id)
                    es = ScanSerializer(scan_objects, many=True)
                else:
                    equals = QuickEqualisation.objects.filter(asickey__equalisationkey = id)
                    es = DashboardQuickEqualisationSerializer(equals, many=True)
            elif stype == 'normal':
                if scan:
                    if scan == 'scanlow':
                        scan_objects = Scan.objects.filter(scannlow__asickey__equalisationkey = id)
                    if scan == 'scanhigh':
                        scan_objects = Scan.objects.filter(scannhigh__asickey__equalisationkey = id)
                    es = ScanSerializer(scan_objects, many=True)
                else:
                    equals = NormalEqualisation.objects.filter(asickey__equalisationkey = id)
                    es = DashboardNormalEqualisationSerializer(equals, many=True)
            elif stype == 'control':
                if scan:
                    scan_objects = Scan.objects.filter(scanhigh__asickey__equalisationkey = id)
                    es = ScanSerializer(scan_objects, many=True)
                else:
                    equals = ControlEqualisation.objects.filter(asickey__equalisationkey = id)
                    es = DashboardControlEqualisationSerializer(equals, many=True)

            response =  JsonResponse(es.data, safe=False)
            return response

        es = EqualisationSerializer(equals, many=True)
        response =  JsonResponse(es.data, safe=False)
        return response

    elif request.method == 'POST':
        equals_data = JSONParser().parse(request)
        es = EqualisationSerializer(data=equals_data)
        if es.is_valid():
            es.save()
            #fills the Equalisation model with 624 asic objects:
            for i in range(624):
                asic_object = AsicEqualisation()
                asic_object.equalisationkey = Equalisation.objects.all().order_by('-id')[0]
                asic_object.asic = i
                asic_object.save()
            return JsonResponse(es.data, status=status.HTTP_201_CREATED)
        return JsonResponse(es.errors, status=status.HTTP_400_BAD_REQUEST)
"""
@api_view(['GET', 'POST'])
def api_asicequalisation(request):
    if request.method == 'GET':
        asicequals = AsicEqualisation.objects.all().order_by('-id')
        print(asicequals)
        asicequals_serializer = AsicEqualisationSerializer(asicequals, many=True)
        print(asicequals_serializer)
        response =  JsonResponse(asicequals_serializer.data, safe=False)
        print(response)
        return response

    elif request.method == 'POST':
        pass
"""

@api_view(['GET', 'POST'])
def api_quickequalisation(request):
    #GET request will be used to for history (perhaps)
    if request.method == 'GET':
        pass
        #quick = QuickEqualisation.objects.all().order_by('-id')
        #quick_serializer = QuickEqualisationSerializer(quick, many=True)
        #response =  JsonResponse(quick_serializer.data, safe=False)
        #return response

    elif request.method == 'POST':
        ls, hs = None, None
        sdata = JSONParser().parse(request)
        
        name = sdata['name']
        del sdata['name']

        
        scanlowserializer = ScanSerializer(data = sdata['scanlow'])
        if scanlowserializer.is_valid():
            ls = scanlowserializer.save()
        del sdata['scanlow']

        scanhighserializer = ScanSerializer(data = sdata['scanhigh'])
        if scanhighserializer.is_valid():
            hs = scanhighserializer.save()
        del sdata['scanhigh']

        asic = sdata['asic']
        quick_serializer = QuickEqualisationSerializer(data=sdata)
        if quick_serializer.is_valid():
            query_object = quick_serializer.save()
            query_object.scanlow = ls
            query_object.scanhigh = hs
            query_object.asickey = AsicEqualisation.objects.filter(asic=asic).filter(equalisationkey__name = name)[0]
            print(ls, hs)
            query_object.save()
            return JsonResponse(quick_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(quick_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def api_normalequalisation(request):
    #GET request will be used to for history (perhaps)
    if request.method == 'GET':
        pass
        #quick = QuickEqualisation.objects.all().order_by('-id')
        #quick_serializer = QuickEqualisationSerializer(quick, many=True)
        #response =  JsonResponse(quick_serializer.data, safe=False)
        #return response

    elif request.method == 'POST':
        ls, hs = None, None
        sdata = JSONParser().parse(request)
        
        name = sdata['name']
        del sdata['name']

        
        scanlowserializer = ScanSerializer(data = sdata['scanlow'])
        if scanlowserializer.is_valid():
            ls = scanlowserializer.save()
        del sdata['scanlow']

        scanhighserializer = ScanSerializer(data = sdata['scanhigh'])
        if scanhighserializer.is_valid():
            hs = scanhighserializer.save()
        del sdata['scanhigh']

        asic = sdata['asic']
        n_serializer = NormalEqualisationSerializer(data=sdata)
        if n_serializer.is_valid():
            query_object = n_serializer.save()
            query_object.scanlow = ls
            query_object.scanhigh = hs
            query_object.asickey = AsicEqualisation.objects.filter(asic=asic).filter(equalisationkey__name = name)[0]
            print(ls, hs)
            query_object.save()
            return JsonResponse(n_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(n_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def api_controlequalisation(request):
    #GET request will be used to for history (perhaps)
    if request.method == 'GET':
        pass
        #quick = QuickEqualisation.objects.all().order_by('-id')
        #quick_serializer = QuickEqualisationSerializer(quick, many=True)
        #response =  JsonResponse(quick_serializer.data, safe=False)
        #return response

    elif request.method == 'POST':
        fs = None
        sdata = JSONParser().parse(request)
        
        name = sdata['name']
        del sdata['name']


        scanfineserializer = ScanSerializer(data = sdata['scan'])
        if scanfineserializer.is_valid():
            fs = scanfineserializer.save()
        del sdata['scan']

        asic = sdata['asic']
        n_serializer = ControlEqualisationSerializer(data=sdata)
        if n_serializer.is_valid():
            query_object = n_serializer.save()
            query_object.scan = fs
            query_object.asickey = AsicEqualisation.objects.filter(asic=asic).filter(equalisationkey__name = name)[0]
            query_object.save()
            return JsonResponse(n_serializer.data, status=status.HTTP_201_CREATED)
        return JsonResponse(n_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET'])
def get_custom(request):
    type = request.GET.get('type')
    db_id = request.GET.get('dbid')
    vp_id = request.GET.get('vpid')
    param = request.GET.get('param')
    scan = request.GET.get('scan')

    print(request)
    if scan == '' or scan == None:
        if type == 'quick':
            equal = QuickEqualisation.objects.filter(asic = vp_id).filter(asickey__equalisationkey__id = db_id)[0]
            equaljson = {param: getattr(equal, param)}
        if type == 'normal':
            equal = NormalEqualisation.objects.filter(asic = vp_id).filter(asickey__equalisationkey__id = db_id)[0]
            equaljson = {param: getattr(equal, param)}
        if type == 'control':
            equal = ControlEqualisation.objects.filter(asic = vp_id).filter(asickey__equalisationkey__id = db_id)[0]
            equaljson = {param: getattr(equal, param)}
    else:
        # scanhigh is a hardcoded name for the scan object in the control scan. TODO: make it more elegant once in production
        if type == 'control':
            got_param = getattr(Scan.objects.filter(scanhigh__asickey__equalisationkey = db_id).filter(asic = vp_id)[0], param)
            equaljson = {param: got_param}

 
    response =  JsonResponse(equaljson, safe=False)
    return response

# History charts
@api_view(['GET'])
def api_scan(request):
    asic = request.GET.get('asic')
    scans = Scan.objects.filter(asic = asic)
    scanserialized = ScanSerializer(scans, many=True)
    response =  JsonResponse(scanserialized.data, safe=False)
    return response
