from django.apps import AppConfig


class PrbsConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'prbs'
