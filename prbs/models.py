from django.db import models

# Create your models here.
class PrbsScan(models.Model):
    date = models.CharField(max_length = 30, default = '')
    comment = models.CharField(max_length = 150)
    name = models.CharField(max_length = 100)
    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

class PrbsLink(models.Model):
    scankey = models.ForeignKey(PrbsScan, on_delete = models.SET_NULL, null = True)
    link = models.PositiveIntegerField(null = True)
    module = models.PositiveIntegerField()
    nbits = models.BigIntegerField(null = True)
    error = models.PositiveIntegerField(null = True)