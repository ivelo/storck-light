from rest_framework import serializers 
from prbs.models import *
 
class PrbsScanSerializer(serializers.ModelSerializer):
    class Meta:
        model = PrbsScan
        fields = ('id',
                  'name',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment',)
        
class PrbsLinkSerializer(serializers.ModelSerializer):
    scankey = serializers.CharField(read_only = True)
    class Meta:
        model = PrbsLink
        fields = ('scankey',
                  'link',
                  'module',
                  'nbits',
                  'error')