from django.urls import path, re_path, include
from prbs import views 
 
urlpatterns = [ 
    re_path(r'^api/prbs$', views.api_prbs),
    re_path(r'^api/prbs/links$', views.api_prbslinks),
]