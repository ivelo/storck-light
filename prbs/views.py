from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from prbs.models import PrbsScan, PrbsLink
from prbs.serializers import *
from rest_framework.decorators import api_view
# Create your views here.

@api_view(['GET', 'POST'])
def api_prbs(request):
    if request.method == 'GET':
        print(request)
        links = PrbsScan.objects.all().order_by('-id')
        
        #used mostly for Dashboard filling:
        id = request.GET.get('id')
        if id:
            links = PrbsLink.objects.filter(scankey = id)
            ls = PrbsLinkSerializer(links, many=True)
            response =  JsonResponse(ls.data, safe=False)
            return response

        ls = PrbsScanSerializer(links, many=True)
        response =  JsonResponse(ls.data, safe=False)
        return response

    elif request.method == 'POST':
        equals_data = JSONParser().parse(request)
        ls = PrbsScanSerializer(data=equals_data)
        if ls.is_valid():
            ls.save()
            return JsonResponse(ls.data, status=status.HTTP_201_CREATED)
        return JsonResponse(ls.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def api_prbslinks(request):
    #GET request will be used to for history
    if request.method == 'GET':
        pass

    elif request.method == 'POST':
        link_data = JSONParser().parse(request)
        print(link_data)
        ls = PrbsLinkSerializer(data = link_data)
        if ls.is_valid():
            query_object = ls.save()
            query_object.scankey = PrbsScan.objects.all().order_by('-id')[0]
            query_object.save()
            return JsonResponse(ls.data, status=status.HTTP_201_CREATED)
        return JsonResponse(ls.errors, status=status.HTTP_400_BAD_REQUEST)