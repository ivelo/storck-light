from django.db import models

class HwRecipe(models.Model):
    #type of recipe
    kind = models.CharField(max_length = 30, default = 'unspecified')
    #date of creation
    date = models.CharField(max_length = 30, default = '')
    #comment displayed in the recipe viewer
    comment = models.CharField(max_length = 150, default = '')

    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = '')

    class Meta:
        #ordering = ["date"]
        app_label = 'recipe'

class HwSubRecipe(models.Model):
    #hardware settings (DAC, phase, ...)
    path = models.CharField(max_length = 255)

    #date of creation
    date = models.CharField(max_length = 30, default = 'no date specified')
    
    #identification
    asic = models.PositiveIntegerField()

    fpath = models.CharField(max_length = 255)

    #basic info to get without looping over actual recipes
    #global_threshold = models.IntegerField()
    data = models.JSONField(default = dict)
    #IDISC = 
    #IKRUM = 
    #IPIXELDAC = 
    #IPREAMP = 
    #VCASDISC = 
    




    recipe = models.ManyToManyField(HwRecipe)

    class Meta:
        ordering = ["asic"]
        app_label = 'recipe'


class MatrixRecipe(models.Model):
    #type of recipe
    kind = models.CharField(max_length = 30, default = 'unspecified')
    #date of creation
    date = models.CharField(max_length = 30, default = '')
    #comment displayed in the recipe viewer
    comment = models.CharField(max_length = 150)

    uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

    class Meta:
        ordering = ["date"]
        app_label = 'recipe'

class MatrixSubRecipe(models.Model):
    #hardware settings (DAC, phase, ...)
    path = models.CharField(max_length = 255)

    #date of creation
    date = models.CharField(max_length = 30, default = 'no date specified')
    
    #identification
    asic = models.PositiveIntegerField()

    #basic info to get without looping over actual recipes
    masks = models.IntegerField()

    fpath = models.CharField(max_length = 150)

    recipe = models.ManyToManyField(MatrixRecipe)

    class Meta:
        ordering = ["asic"]
        app_label = 'recipe'
