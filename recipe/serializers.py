from rest_framework import serializers 
from recipe.models import *
 
class MatrixRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = MatrixRecipe
        fields = ('id',
                  'kind',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment') 

#TODO: check if this serializer is really needed
class MatrixSubRecipeLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = MatrixSubRecipe
        fields = ('id',
                  'recipe',
                  'asic') 

class MatrixSubRecipeSerializer(serializers.ModelSerializer):
    recipe = MatrixRecipeSerializer(many = True, read_only = True)
    class Meta:
        model = MatrixSubRecipe
        fields = ('id',
                  'date',
                  'asic',
                  'masks',
                  'fpath',              
                  'recipe') 

class HwRecipeSerializer(serializers.ModelSerializer):
    class Meta:
        model = HwRecipe
        fields = ('id',
                  'kind',
                  'date',
                  'uploaded_by',
                  'uploaded_from',
                  'comment') 

#TODO: check if this serializer is really needed
class HwSubRecipeLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = HwSubRecipe
        fields = ('id',
                  'recipe',
                  'asic') 

class HwSubRecipeSerializer(serializers.ModelSerializer):
    recipe = HwRecipeSerializer(many = True, read_only = True)
    class Meta:
        model = HwSubRecipe
        fields = ('id',
                  'date',
                  'asic',
                  'data',
                  'fpath',                 
                  'recipe') 

    #def to_representation(self, instance):
    #    self.fields['recipe'] = HwRecipeSerializer(read_only = True)
    #    return super().to_representation(instance)