from django.shortcuts import render

from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from recipe.models import HwRecipe
from recipe.serializers import *
from rest_framework.decorators import api_view


@api_view(['GET', 'POST'])
def api_hwrecipe(request):
    print('Entering hw')
    if request.method == 'GET':
        print(request)
        recipes = HwRecipe.objects.all().order_by('-id')
        
        kind = request.GET.get('kind', None)
        #print(kind)
        if kind is not None:
            recipes = recipes.filter(kind__icontains=kind)
        
        recipes_serializer = HwRecipeSerializer(recipes, many=True)
        #print(JsonResponse(recipes_serializer.data, safe=False))
        response =  JsonResponse(recipes_serializer.data, safe=False)
        #response["Access-Control-Allow-Origin"] = "*"
        #response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
        #response["Access-Control-Max-Age"] = "1000"
        #response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
        return response

    elif request.method == 'POST':
        print(request)
        print('Addding a recipe abstract object')
        recipe_data = JSONParser().parse(request)
        recipe_serializer = HwRecipeSerializer(data=recipe_data)
        print(recipe_data, recipe_serializer)
        if recipe_serializer.is_valid():
            recipe_serializer.save()
            return JsonResponse(recipe_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(recipe_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['POST'])
def api_hwrecipelink(request):
    if request.method == 'POST':
        print('In api_hwrecipelink', request)
        recipe_data = JSONParser().parse(request)
        #recipe_serializer = HwSubRecipeLinkSerializer(data=recipe_data)
        print(recipe_data, recipe_data['recipe'])
        if 1: #recipe_serializer.is_valid():
            #To verify if ordering by ID will not generate issues:
            last = HwRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[0]
            #second_last = HwRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[1]
            #lastsub = HwSubRecipe.objects.filter(re = recipe_data['recipe'], asic = recipe_data['asic']).order_by('-id')[0]
            second_last = HwRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[1]
            print(second_last, second_last.hwsubrecipe_set.all())
            second_last_sub = second_last.hwsubrecipe_set.filter(asic = recipe_data['asic']).order_by('-id')[0]
            second_last_sub.recipe.add(last)
            #last.recipe.add(HwRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[0])
            return JsonResponse({'status': 'Successfully linked'}, status=status.HTTP_201_CREATED) 
        return JsonResponse({'status': 'Should never go here (to remove later)'}, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def api_hwsubrecipe(request):
    print('A check one', request)
    if request.method == 'GET':
        #how you access params in url
        print(request.GET.get('asic'))
        asic = request.GET.get('asic')
        #full = request.GET.get('full')
        id = request.GET.get('id')
        subid = request.GET.get('subid')

        if asic is not None:
            recipes = HwSubRecipe.objects.filter(asic = request.GET.get('asic'))
        elif id is not None:
            recipes = HwRecipe.objects.filter(id = id)[0].hwsubrecipe_set.all()
            if subid is not None:
                recipes = recipes.filter(id = recipes[0].id + int(subid))
        else:
            recipes = HwSubRecipe.objects.all()
        
        
        kind = request.GET.get('kind', None)
        print('Control check two', kind)
        if kind is not None:
            recipes = recipes.filter(kind__icontains=kind)
        
        recipes_serializer = HwSubRecipeSerializer(recipes, many=True)
        print('Control check three', recipes_serializer.data)
        #print(JsonResponse(recipes_serializer.data, safe=False))
        return JsonResponse(recipes_serializer.data, safe=False)

    if request.method == 'POST':
        print(request)
        print('Adding a subrecipe to recipe object')
        recipe_data = JSONParser().parse(request)
        recipe_serializer = HwSubRecipeSerializer(data=recipe_data)
        print(recipe_serializer)
        print(recipe_data, recipe_data['recipe'])
        if recipe_serializer.is_valid():
            query_object = recipe_serializer.save()
            queryset = HwRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[0]
            print(queryset)
            query_object.recipe.add(queryset)
            return JsonResponse(recipe_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(recipe_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

@api_view(['GET', 'POST'])
def api_matrixrecipe(request):
    print('Entering matrix')
    if request.method == 'GET':
        print(request)
        recipes = MatrixRecipe.objects.all().order_by('-id')
        
        kind = request.GET.get('kind', None)
        if kind is not None:
            recipes = recipes.filter(kind__icontains=kind)
        
        recipes_serializer = MatrixRecipeSerializer(recipes, many=True)
        response =  JsonResponse(recipes_serializer.data, safe=False)
        return response

    elif request.method == 'POST':
        print(request)
        print('Addding a recipe abstract object')
        recipe_data = JSONParser().parse(request)
        recipe_serializer = MatrixRecipeSerializer(data=recipe_data)
        if recipe_serializer.is_valid():
            recipe_serializer.save()
            return JsonResponse(recipe_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(recipe_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'POST'])
def api_matrixsubrecipe(request):
    print('A check one', request)
    if request.method == 'GET':
        #how you access params in url
        vpid = request.GET.get('asic')
        #full = request.GET.get('full')
        dbid = request.GET.get('id')
        param = request.GET.get('param')
        subid = request.GET.get('subid')

        if param is not None and dbid is not None and vpid is not None:
            recipe = MatrixSubRecipe.objects.filter(asic = vpid).filter(recipe__id = dbid)[0]
            paramjson = {param: getattr(recipe, param)}
            return JsonResponse(paramjson, safe=False)
        if vpid is not None:
            recipes = MatrixSubRecipe.objects.filter(asic = vpid)
        elif dbid is not None:
            recipes = MatrixRecipe.objects.filter(id = dbid)[0].matrixsubrecipe_set.all()
            if subid is not None:
                recipes = recipes.filter(id = recipes[0].id + int(subid))
        else:
            recipes = MatrixSubRecipe.objects.all()
        
        kind = request.GET.get('kind', None)
        print('Control check two', kind)
        if kind is not None:
            recipes = recipes.filter(kind__icontains=kind)
        
        recipes_serializer = MatrixSubRecipeSerializer(recipes, many=True)
        print('Control check three', recipes_serializer.data)
        #print(JsonResponse(recipes_serializer.data, safe=False))
        return JsonResponse(recipes_serializer.data, safe=False)

    if request.method == 'POST':
        print(request)
        print('Adding a subrecipe to recipe object')
        recipe_data = JSONParser().parse(request)
        recipe_serializer = MatrixSubRecipeSerializer(data=recipe_data)
        print(recipe_data, recipe_data['recipe'])
        if recipe_serializer.is_valid():
            query_object = recipe_serializer.save()
            queryset = MatrixRecipe.objects.filter(kind = recipe_data['recipe']).order_by('-id')[0]
            print(queryset)
            query_object.recipe.add(queryset)
            return JsonResponse(recipe_serializer.data, status=status.HTTP_201_CREATED) 
        return JsonResponse(recipe_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
