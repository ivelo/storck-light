from django.db import models

# Create your models here.
class Run(models.Model):
    runid = models.IntegerField()
    partitionid = models.IntegerField(null = True) #not sure if really needed
    runtype = models.CharField(max_length = 30, null = True)
    partitionname = models.CharField(max_length = 30, null = True)
    destination = models.CharField(max_length = 30, null = True)
    beamenergy = models.IntegerField(null = True)
    
    fillid = models.IntegerField(null = True)
    startlumi = models.FloatField(null = True)
    endlumi = models.FloatField(null = True)
    activity = models.CharField(max_length = 50, null = True)
    #run_state - what is run_state?
    #state = "MIGRATED" - what is state?
    LHCState = models.CharField(max_length = 30, null = True)
    starttime = models.CharField(max_length = 30, default = '', null = True)
    endtime = models.CharField(max_length = 30, default = '', null = True)
    #calib settings = 30|61|46|78
    #SMOG and next line SMOGLumi
    avLumi = models.FloatField(null = True)
    avMu = models.FloatField(null = True)

    veloOpening = models.FloatField(null = True) # what is that number exactly? ~54 when open, 0.000x when closed
    veloPosition = models.CharField(max_length = 30, default = '', null = True)

    prev_runid = models.IntegerField(null = True)
    next_runid = models.IntegerField(null = True)
    #comment displayed in the recipe viewer
    comment = models.CharField(max_length = 300, default = '')
    analysed = models.BooleanField(default = False)

    #uploaded_by = models.CharField(max_length = 30, default = 'anonymous')
    #uploaded_from = models.CharField(max_length = 50, default = 'anonymous')

    class Meta: 
        app_label = 'run'