from rest_framework import serializers 
from run.models import *

class RunSerializer(serializers.ModelSerializer):
    class Meta:
        model = Run
        fields = ('id',
                  'runid',
                  'partitionid',
                  'runtype',
                  'partitionname',
                  'destination',
                  'beamenergy',
                  'fillid',
                  'startlumi',
                  'endlumi',
                  'activity',
                  'LHCState',
                  'starttime',
                  'endtime',
                  'avLumi',
                  'avMu',
                  'veloOpening',
                  'veloPosition',
                  'prev_runid',
                  'next_runid',
                  'comment',
                  'analysed')