from django.urls import path, re_path, include
from run import views 
 
urlpatterns = [ 
    re_path(r'^api/run$', views.api_run),
    re_path(r'^api/run/latest', views.api_run_latest),
    #re_path(r'^api/recipe/subrecipe/hw/link$', views.api_hwrecipelink),
    #re_path(r'^api/recipe/matrix$', views.api_matrixrecipe),
    #re_path(r'^api/recipe/subrecipe/hw$', views.api_hwsubrecipe),
    #re_path(r'^api/recipe/subrecipe/matrix$', views.api_matrixsubrecipe),
]
