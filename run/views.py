from django.http.response import JsonResponse
from rest_framework.parsers import JSONParser 
from rest_framework import status
 
from run.serializers import *
from rest_framework.decorators import api_view

# Create your views here.
@api_view(['GET', 'POST'])
def api_run(request) -> dict:
    partitionname = request.GET.get('partitionname', None)
    if request.method == 'GET':
        print(request)
        runs = Run.objects.filter(partitionname=partitionname).order_by('-runid')
        
        #kind = request.GET.get('kind', None)
        #print(kind)
        #if kind is not None:
        #    recipes = recipes.filter(kind__icontains=kind)
        
        run_serializer = RunSerializer(runs, many=True)
        response =  JsonResponse(run_serializer.data, safe=False)
        return response

    elif request.method == 'POST':
        print('Addding a run object')
        run_data = JSONParser().parse(request)

        #check if already exists to not duplicate the previous latest run
        existing_runs = Run.objects.filter(runid=run_data['runid'])
        if len(existing_runs) != 0:
            existing_run = existing_runs[0]
            if 'next_runid' in run_data:
                existing_run.next_runid = run_data['next_runid']
                existing_run.save()
            return JsonResponse(None, safe = False)

        run_serializer = RunSerializer(data=run_data)
        #print(run_data, run_serializer)
        if run_serializer.is_valid():
            run_serializer.save()
            return JsonResponse(run_serializer.data, status=status.HTTP_201_CREATED)
        
        return JsonResponse(run_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#GET the latest run given partitionname
@api_view(['GET'])
def api_run_latest(request) -> dict:
    partitionname = request.GET.get('partitionname', None)
    latest = Run.objects.filter(partitionname = partitionname).order_by('-runid')[0]
    latest_serialized = RunSerializer(latest)
    return JsonResponse(latest_serialized.data, status = status.HTTP_201_CREATED)
